package az.ingress.customer.controller;

import az.ingress.customer.model.Customer;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @GetMapping("/greeting")
    public String greetCustomer(@RequestParam String name,@RequestHeader(value = "Accept-Language") String language ) {
        return switch (language) {
            case "az" -> "Salam " + name;
            case "ru" -> "Привет " + name;
            case "en" -> "Hello  " + name;
            default -> "Hello "+ name;
        };
    }


    @PostMapping("/create")
    public Customer createCustomer(@RequestBody Customer customer) {
        System.out.println("CreateCustomer");
        return customer;
    }

    @PutMapping("/update/{id}")
    public Customer updateCustomer(@PathVariable Integer id) {
        System.out.println("UpdateCustomer");
        Customer customer = Customer.builder().id(id).name("Huseyn").surname("Guliyev").address("Khanlar str").build();
        return customer;
    }

    @DeleteMapping("/delete/{id}")
    public Customer deleteCustomer(@PathVariable Integer id) {
        System.out.println("DeleteCustomer");
        Customer customer = Customer.builder().id(id).name("Huseyn").surname("Guliyev").address("Khanlar str").build();
        return customer;
    }

}
