package az.ingress.customer.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Customer {
    private Integer id;
    private String name;
    private String surname;
    private String address;
}
